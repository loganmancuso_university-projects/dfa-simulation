/****************************************************************
  * 'dfa.h'
  *
  * Author/CopyRight: Mancuso, Logan
  * Last Edit Date: 11-27-2018--13:30:41
  *
 **/

#ifndef DFA_H
#define DFA_H

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>

#include "../../Utilities/scanline.h"

class DFA {
  public:
  //construct/deconstruct
    DFA();
    virtual ~DFA();
  //functions 
	  void set_number_of_states( int states );
		void set_final_states( std::vector<std::string> a_string );
    void set_alphabet( std::string a_string );
		void set_transition_table( int state, char reading, int next_state );
		void set_language( std::string input, bool result );
		void set_minimized_transition_table( int state, char reading, int next_state );

		int get_number_of_states();
		std::vector<int> get_final_states();
		std::vector<char> get_alphabet();
		char get_alphabet_at( int i );
    std::map<std::pair<int, char>, int> get_transition_table();
		std::vector<std::pair<std::string, std::string>> get_language();
    std::map<std::pair<int, char>, int> get_minimized_transition_table();

    void in_language( std::string input );
		
		std::string print_dfa();
    std::string print_simulation();
		std::string print_minimization();

  private:
    int number_of_states_;
    std::vector<int> final_states_; 
    std::vector<char> alphabet_ = {}; //will be chars, compare ascii values
		std::map<std::pair<int, char>, int> transition_table_; //current state, reading char = next state
    std::vector<std::pair<std::string, std::string> > language_; //vector of pairs 
		std::map<std::pair<int, char>, int> minimized_transition_table_; 
};//end class

#endif //DFA_H

 /****************************************************************
  * End 'dfa.h'
 **/
