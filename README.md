# Program: 'Deterministic Finite Automata'

Author/CopyRight: Mancuso, Logan  
Language: C++  
Last Edit Date: 11-27-2018--13:31:25

## Usage
The program `Aprog` takes in 1 or 2 files, and a flag: -S or -M the order is: DFA config, input_strings.
  * `x0.in` is for the DFA specifications: and follows the style outlined in `Programming Assignment.pdf` (Shown Below).
    > Number of states: 5   
    > Accepting states: 1 4  
    > Alphabet: 01
    > 0 1  
    > 2 3  
    > 4 0  
    > 1 2  
    > 3 4  
* `x2.in` is for 'Simulating a DFA' project and the sets of strings to test will be placed in that file.

## Building and Running the Code
1) Run the build.sh script from the working source files directory. The script will error check that you are in the correct folder before running. 
`cd WorkingDirectory/SourceFiles && ../.././build.sh`
2) run the Run.sh script to clean the Test directory and compile for the computer it is running on. 
`../.././Run.sh` (the script is designed to be run from the current working directory so that the programmer does not need to constantly move directories when coding). 
3) The program will then remove the current test directory, copy all files from working in to the new test directory and append a header to the files. " THIS IS A TEST FILE CHANGES WILL NOT BE SAVED " so as to warn the developer not to add code to these files. 
4) after it is done compiling the script will halt if errors are returned; errors are printed to sh.log in the IOFiles directory.

## Sources:
Utilities Directory is a compilation of work created by outside sources. Dr. Duncan Buell created the 'utilities', 'scanner', and 'scanline' resources that are used for file input/output, and string manipulation. All code in the working directory is of my own work. 

