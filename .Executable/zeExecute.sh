# *************************************************************
# 'zeExecute.sh'
# this program will execute the Aprog in the test directory
#
# Author/CopyRight: Mancuso, Logan
# Last Edit Date: 11-29-2017--11:04:36
# *************************************************************

#!/bin/bash
cd ../../IOFiles
rm *.out *.log
echo "Descend into 'TestDirectory' directory"
cd ../.TestDirectory/
include=../../IOFiles
#
for item in *
do
  echo " "
  echo "EXECUTING" $item
  cd $item
	if [ $1 = "-S" ]; then
    ./Aprog $1 $2 $3 # $include/x1.in $include/x2.in
	elif [ $1 = "-M" ]; then
    ./Aprog $1 $2 # $include/x1.in $include/x2.in
	else
	  echo "invalid flag"
	fi
  cd ..
echo "EXECUTION COMPLETE"
done
echo "Return To 'WorkingDirectory' directory"
cd ../WorkingDirectory/SourceFiles
echo " "
# *************************************************************
# End 'zeExecute.sh'
# *************************************************************

